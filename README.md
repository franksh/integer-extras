# integer-extras

Provides `Data.Integer.Extras` with some functionality surrounding integers.

There are two functions in particular I feel were forgotten when Haskell formed
their standard library:

- `nbits`: A primitive (i.e. low-level and fast) function for finding the "bit
  size" of an Integer (i.e. rounded up `log2`). I have no idea why this
  universally useful function is missing from `Data.Bits`. See also [Python's
  bit_length()](https://docs.python.org/3/library/stdtypes.html#int.bit_length).

- `gcdext`: Extended GCD giving Bezout coefficients from the GCD algorithm. This
  algorithm lies at the very core of doing modular arithmetic as well as certain
  lattice arrangements, so its speed is tantamount -- and GMP's implementations
  is one of the fastest known. Yet it's hidden away in
  `GHC.Integer.GMP.Internals`. (And I'm not sure what to do for GHC 9 if they
  want to switch away from GMP? It will be really hard to match GMP's speed.)
  
I've included various other things that _I_ think ought to be available as
low-level primitives in programming languages that support arbitrary precision
integers, although these might be less generally useful. This includes:

- `clMul`, `clMul64`: carry-less multiplication for integers. This is core for
  doing arithmetic in 𝔽_2[X] (think of it as polynomials using single bits as
  coefficients), used in a lot of cryptographic primitives.
- `invMod` for finding modular inverses, though it can be constructed from
  `gcdext` above.
- `powMod` for doing fast exponentiation under a modulus. Also a useful
  primitive in many cryptographic algorithms.
- `intLog`, `intLog2`: integer logarithms, i.e. answering the question "how many
  digits does a number have in base `b`?"
- `intSqrtRem`, `intRootRem`: integer square roots (or nth roots) which are
  important in many algorithms that work in O(√n) time or space, for giving a
  bound or initial guess.
- `isPrime`, `nextPrime`, `prevPrime`: if number theory is music then primes are
  the notes. I just think any programming language using GMP or GMP-like ought
  to expose such functions.
- `oddPow` and `divPow`: removing a given factor from a number, e.g. for
  splitting a number like `n = 2^k * q` for odd `q`.
  
I'm not sure what will happen with GHC 9s push to switch away from GMP(?) -- I
personally am very worried it will lead to more neglect of fast low level
support for integer operations useful in cryptography and number theory.
