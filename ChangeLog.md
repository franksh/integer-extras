# Changelog for integer-extras

## [0.2.0] - 2022-01-21

- Renamed `clmul64` to `clMul64`.
- Added `clMul :: Integer -> Integer -> Integer`.
- Added `intToWords :: Integer -> [Word]`.

## [0.1.0] - 2022-01-20

- Initial release.
