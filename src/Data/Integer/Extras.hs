{-# LANGUAGE MagicHash #-}
{-# LANGUAGE UnboxedTuples #-}
{-# LANGUAGE BangPatterns #-}

-- | Various extra integer functionality.
--
-- I've found myself missing a few things for working with integers that I
-- consider pretty essential. These seems to have slipped the minds of Haskell's
-- standard library committee.
--
-- The most prominent missing functionality are:
--
--  * being able to query the "bit length" of an @Integer@ value, like
--    @int.bit_length()@ in Python.
--  * a function doing extended Euclidean GCD, suitable for finding modular inverses.
--  * or more generally, a way to do modular exponentiation (i.e. @powmod@ in Python).
--
-- Other functionality which is sorely lacking even from
-- 'GHC.Integer.GMP.Internals' is integer roots (@mpz_rootrem@ and
-- @mpz_sqrtrem@), and Jacobi/Kronecker symbol (@mpz_jacobi@) which has a wide
-- range of number-theoretic applications.
--
-- All of these are pretty important if doing any sort of basic number-theoretic
-- or cryptographic algorithms in pure Haskell.
--
-- A way to do carry-less multiplication (using the CPU instructions if
-- available) is also something I personally wish was more prevalent, though few
-- languages possess this.

module Data.Integer.Extras where

import Prelude as P
import Data.Integer.Extras.CLMul
import GHC.Integer
import GHC.Integer.Logarithms
import GHC.Exts
import GHC.Integer.GMP.Internals
import GHC.Word
import Data.WideWord.Word128
import Numeric.GMP.Raw.Safe
import Numeric.GMP.Utils
import System.IO.Unsafe
import Data.Bits

-- | Number of bits needed to represent the absolute value of an integer.
--
-- This works similarly to Python's 'int.bit_length()'. It gives the minimum
-- number of bits needed to represent the absolute value of the given integer.
-- 'nbits 0' is defined as 0.
--
-- >>> nbits 3
-- 2
-- >>> nbits $ 2^31
-- 32
-- >>> nbits (2^31 - 1)
-- 31
nbits :: Integer -> Int
nbits = succ . intLog2 . absInteger

-- | Integer base-2 logarithm of a positive integer.
--
-- This is similar to 'nbits' but truncates instead of rounding up. 'intLog2 0'
-- is defined as -1 (all bits set). This is faster and more accurate than
-- converting to 'Double' and using 'logBase' in Prelude, which would give
-- inaccurate result on large integers.
intLog2 :: Integer -> Int
intLog2 n | n < 0 = error "invalid domain"
intLog2 n = I# (integerLog2# (absInteger n))

-- | Integer floor of 'n' in base 'b' of an integer.
intLog :: (Integral b) => b -> Integer -> Int
intLog b n
  | b <= 1 = error "base must be > 1"
  | otherwise = I# (integerLogBase# (toInteger b) (absInteger n))

-- | Extended GCD: returns @(g, s, t)@ such that @g = s*x + t*y@.
--
-- This algorithm is integral to all things involving modular arithmetic. I'm
-- not sure how Haskell missed it.
gcdext :: Integer -> Integer -> (Integer, Integer, Integer)
gcdext x y = case gcdExtInteger x y
  of (# g, s #) -> (g, s, (g - s * x) `divInteger` y)

-- | Probabilistic prime test (with extremely high confidence).
isPrime :: Integer -> Bool
isPrime n = case testPrimeInteger n 25# of
  0# -> False
  _ -> True

-- | Returns the least (positive) prime that is larger than the given integer.
nextPrime :: Integer -> Integer
nextPrime = nextPrimeInteger

-- | Returns the largest (positive) prime that is smaller than the given
-- integer.
--
-- If no such prime exists because the input is less than or equal to 2, it will
-- simply return 2.
prevPrime :: Integer -> Integer
prevPrime n | n <= 3 = 2
prevPrime n = firstPrime [n', n'-2 ..]
  where
    n' = if even n then pred n else n - 2
    firstPrime = head . filter isPrime

-- | Expose GMP's square integer operation.
--
-- NOTE: it turns out (through benchmarking) that this does not provide any gain
-- over simply writing @x * x@. (For small bit sizes it might be very marginally
-- faster, but for large bit sizes it actually seems slower‽)

-- intSqr' :: Integer -> Integer
-- intSqr' = sqrInteger

-- | Some more exposed functions from GHC's GMP internals.
--
-- NOTE: these also turn out no faster than the regular versions in Prelude.
-- gcd' :: Integer -> Integer -> Integer
-- gcd' = gcdInteger
-- lcm' :: Integer -> Integer -> Integer
-- lcm' = lcmInteger

-- | Sort of like the ceiling version of 'intLog'.
--
-- NOTE: According to the docs it is also not guaranteed to be accurate, but
-- might return a number +1 off from the correct answer?
-- digitsInBase :: Int -> Integer -> Word
-- digitsInBase b _ | b < 2 || b > 256 = error "base must be in range [2,256]"
-- digitsInBase (I# b) a = W# (sizeInBaseInteger (absInteger a) b)

-- | Inverse of first argument modulo the second.
--
-- Evaluates to 0 if no inverse exist.
--
-- >>> invmod 7 257
-- 147
-- >>> invmod (-2) 777 * (-2) `mod` 777
-- 1
invMod :: Integer -> Integer -> Integer
invMod = recipModInteger

-- | Raises a number to the given power under some modulus.
--
-- If the exponent is negative it uses 'invMod' on the base before calculating
-- the power, as @a^-n = (1/a)^n@.
powMod :: Integer -> Integer -> Integer -> Integer
powMod _ _ m | m < 1 = error "domain error"
powMod a e m | e < 0 = powModInteger (recipModInteger a m) (negateInteger e) m
powMod a e m = powModInteger a e m

-- | Jacobi symbol. Returns -1 for quadratic non-residues and 1 for quadratic
-- residues.
--
-- See 'https://en.wikipedia.org/wiki/Jacobi_symbol'
--
-- This also implements the Kronecker extension so it works for even numbers
-- too.
jacobi :: Integer -> Integer -> Int
jacobi a b = fromIntegral
  $ unsafePerformIO
  $ withInInteger a
  $ \a' -> withInInteger b
  $ \b' -> mpz_jacobi a' b'

-- | Truncated integer square root with remainder.
--
-- >>> let (r, b) = intSqrtRem a in r^2 + b == a
-- True
intSqrtRem :: Integer -> (Integer, Integer)
intSqrtRem n | n < 0 = error "invalid domain"
intSqrtRem n = unsafePerformIO
  $ withOutInteger
  $ \sqrt' -> withOutInteger_
  $ \rem' -> withInInteger n
  $ \n' -> mpz_sqrtrem sqrt' rem' n'

-- | Truncated integer root with remainder.
--
-- >>> let (r, b) = intRootRem 7 a in r^7 + b == a
-- True
intRootRem :: (Integral r) => r -> Integer -> (Integer, Integer)
intRootRem r n | n < 0 || r < 1 = error "invalid domain"
intRootRem r n = unsafePerformIO
  $ withOutInteger
  $ \root' -> withOutInteger_
  $ \rem' -> withInInteger n
  $ \n' -> mpz_rootrem root' rem' n' (fromIntegral r)

-- | Trailing zero bits.
--
-- The argument is interpreted as an absolute value. Evaluates to -1 if there
-- are no 1-bits (n == 0).
--
-- >>> intTrailingZeros 60
-- 2
intTrailingZeros :: Integer -> Int
intTrailingZeros 0 = -1
intTrailingZeros n | n < 0 = intTrailingZeros $ -n
intTrailingZeros n
  = fromIntegral
  $ unsafePerformIO
  $ withInInteger n
  $ \n' -> mpz_scan1 n' 0

-- | Remove factors until it is coprime with the given number.
--
-- >>> makeCoprime 10 24
-- 3
makeCoprime :: (Integral a) => a -> a -> a
makeCoprime m a = if g <= 1 then a else makeCoprime m (a `div` g)
  where g = gcd a m

-- | Split a positive number into an odd part and a power of two.
--
-- Equivalent to 'divPow 2' but faster for Integers.
--
-- >>> oddPow 40
-- (5, 3)
oddPow :: Integer -> (Integer, Int)
oddPow 0 = (0, 0)
oddPow n = (n `unsafeShiftR` b, b)
  where b = intTrailingZeros n


-- | Divide out the maximum power of a factor that goes into a number, returning
-- the resulting quotient and the exponent.
--
-- >>> divPow 5 (5^7 * 19)
-- (19, 7)
divPow :: (Integral a) => a -> a -> (a, a)
divPow p _ | abs p <= 1 = error "domain error"
divPow _ 0 = (0,0)
divPow p n = go n 0
  where
    go !m !k = let (q,r) = divMod m p in
      if r == 0 then go q (k+1) else (m, k)

-- | 64-bit carry-less multiplication (X86-64 CPU accelerated).
--
-- This works as if multiplication was done in a ring where addition was done
-- bitwise with no carry (i.e. where addition is the same as XOR).
--
-- This is intended as a primitive to implement multiplication of polynomials
-- over GF_2.
--
-- Only works on x86-64 platforms.
clMul64 :: Word64 -> Word64 -> Word128
clMul64 = binaryPolyMulCallingTwice

-- | Converts an 'Integer' into its representation as a list of 'Word64'.
--
-- Ignores the sign of the integer.
intToWord64s :: Integer -> [Word64]
intToWord64s bn' = case bn' of
  (S# i) -> [W64# (int2Word# i)]
  (Jp# bn) -> go bn
  (Jn# bn) -> go bn
  where
    go bn = [W64# (indexBigNat# bn k) | (I# k) <- [0 .. I# (sizeofBigNat# bn -# 1#)]]

-- | Carry-less multiplication of integers (using clMul64).
--
-- This ignores the sign of the Integer. (Should be Natural?)
--
-- NOTE/TODO: this is just a filler function for convenience that does "dumb"
-- @O(n^2)@ multiplication of @Word64@-sized digits. Should be implemented
-- better with Karatsuba/Toom etc.
clMul :: Integer -> Integer -> Integer
clMul x y = clMul' (intToWord64s x) (intToWord64s y)
  where
    clMul' (x:xs) ys = stepSum (clMul64 x <$> ys) `xor` unsafeShiftL (clMul' xs ys) 64
    clMul' [] _ = 0
    stepSum [] = 0
    stepSum (x:xs) = toInteger x `xor` unsafeShiftL (stepSum xs) 64
