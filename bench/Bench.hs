import Criterion.Main
import Data.Integer.Extras
import Data.Bits

argSmall :: Integer
argSmall = 0xdeadbeef12345678
-- Brainpool P-384t1 curve parameter.
argMedium :: Integer
argMedium = 0x8cb91e82a3386d280f5d6f7e50e641df152f7109ed5456b412b1da197fb71123acd3a729901d1a71874700133107ec53
-- Random 2048-bit RSA modulus.
argLarge :: Integer
argLarge = 0x6c02d54d86e55c2c0b9fc97dd8ac19cc0696950abbe3c60f343d0cafefacb69999cac5830e3ce88dd0c8aeb46cd90657e40989ce6c13217df92106fe216824e61abb102c4b28b6442411e35cc530b18cb8068937d8fd525b8c669195962a92b49a42d3715326eca416f982bb9279d8061954a7de2dd5b659ab0f6c69beec1bb3ec08af9b1281b25bcc5ae7fcf8a7a106b2fa9c9147994d5edb539adc91073f6ed82776ab8927aeaf2682947d1a841231bcb7928813c9ac6f69c2e3cac0fd7c2e3689cb8e474f4c604f5657f1595ce1fe765ea4fea58d6e7f37d4fdac0ab815aa0cde9864de5467cacac4cf9a61824fc943ae1807158f1c376a5dd8e8dbe896a3

randomInteger :: Integer
randomInteger = 179741004495208123591715169329395730618511697352484417183769657418032357983089770954336171362922027825279964909221663389724454965908361227194253412383286027376086016703234717068018001859807823325098411339805960742832091567824135737810673084410989367585714190468591292748703936875552058847739292631974259949087

b_log2 n =
  bgroup ("intLog2 (" ++ show (nbits n) ++ " bits)")
    [ bench "intLog2" $ nf intLog2 n
    , bench "Prelude.logBase (v. inaccurate)" $ nf (\x -> logBase (2.0 :: Double) (fromIntegral x)) n
    , bench "naive Data.Bits" $ nf (\x -> head $ [k | k <- [0..], unsafeShiftR x k <= 1]) n
    ]
b_log n =
  bgroup ("log' (" ++ show (nbits n) ++ " bits)")
    [ bench "intLog" $ nf (intLog 7) n
    , bench "Prelude.logBase (v. inaccurate)" $ nf (\x -> logBase (7.0 :: Double) (fromIntegral x)) n
    ]
b_gcdext n =
  bgroup ("gcdext' (" ++ show (nbits n) ++ " bits)")
    [ bench "gcdext" $ nf (gcdext randomInteger) n
    -- , bench "Prelude.logBase (v. inaccurate)" $ nf (\x -> logBase (7.0 :: Double) (fromIntegral x)) n
    ]
b_isprime n =
  bgroup ("isPrime' (" ++ show (nbits n) ++ " bits)")
    [ bench "isPrime" $ nf isPrime n
    ]
b_nextprime n =
  bgroup ("nextPrime' (" ++ show (nbits n) ++ " bits)")
    [ bench "nextPrime" $ nf nextPrime n
    ]
b_prevprime n =
  bgroup ("prevPrime' (" ++ show (nbits n) ++ " bits)")
    [ bench "prevPrime" $ nf prevPrime n
    ]
b_digits n =
  bgroup ("digitsInBase' (" ++ show (nbits n) ++ " bits)")
    [ bench "digitsInBase" $ nf (\n -> digitsInBase n 64) n
    ]
b_invmod n =
  bgroup ("invMod' (" ++ show (nbits n) ++ " bits)")
    [ bench "invMod _ m" $ nf (\n -> invMod randomInteger n) n
    , bench "invMod a _" $ nf (\n -> invMod n randomInteger) n
    ]
b_powmod n =
  bgroup ("powMod' (" ++ show (nbits n) ++ " bits)")
    [ bench "powMod _ e _" $ nf (\n -> powMod 7 n randomInteger) n
    , bench "powMod _ _ m" $ nf (\n -> powMod 7 randomInteger n) n
    ]
b_jacobi n =
  bgroup ("powMod' (" ++ show (nbits n) ++ " bits)")
    [ bench "jacobi a _" $ nf (\n -> jacobi n 0x91919191913) n
    , bench "jacobi _ b" $ nf (\n -> jacobi 0x91919191913 n) n
    ]
b_intsqrt n =
  bgroup ("intSqrtRem' (" ++ show (nbits n) ++ " bits)")
    [ bench "intSqrtRem" $ nf intSqrtRem n
    ]
b_introot n =
  bgroup ("intRootRem' (" ++ show (nbits n) ++ " bits)")
    [ bench "intRootRem" $ nf (intRootRem 13) n
    ]
-- b_introot n =
--   bgroup ("clmul64' (" ++ show (nbits n) ++ " bits)")
--     [ bench "clmul64" $ nf (clmul64 0xdeadbeef87654321) n
--     ]

bs k =
  bgroup name
    [ b_log2 arg
    , b_log arg
    , b_gcdext arg
    , b_isprime arg
    , b_nextprime arg
    , b_prevprime arg
    , b_digits arg
    , b_invmod arg
    , b_powmod arg
    , b_jacobi arg
    , b_intsqrt arg
    , b_introot arg
    ]
  where (name, arg) = case k of
          0 -> ("small", argSmall)
          1 -> ("medium", argMedium)
          _ -> ("large", argLarge)


main :: IO ()
main = do
  defaultMain
    [ bs 0
    , bs 1
    , bs 2
    ]
