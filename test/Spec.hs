{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
import Test.QuickCheck
import Data.Integer.Extras
import Data.Word
import Data.Bits
import Data.Foldable
import Control.Monad

newtype LargeInteger = LargeInteger Integer deriving (Show, Eq, Ord, Num)
newtype LargeNonNegative = LargeNonNegative Integer deriving (Show, Eq, Ord, Num)
newtype LargePositive = LargePositive Integer deriving (Show, Eq, Ord, Num)

fromWords :: [Word8] -> Integer
fromWords = foldl' go 0
  where
    go :: Integer -> Word8 -> Integer
    go acc w = (acc `shiftL` 8) + fromIntegral w

instance Arbitrary LargeNonNegative where
  arbitrary = do
    NonNegative n <- arbitrary
    words <- replicateM n arbitrarySizedBoundedIntegral
    pure $ LargeNonNegative (fromWords words)

instance Arbitrary LargePositive where
  arbitrary = do
    NonNegative n <- arbitrary
    first <- arbitrarySizedBoundedIntegral `suchThat` (> 0)
    rest <- replicateM n arbitrarySizedBoundedIntegral
    pure $ LargePositive (fromWords $ first : rest)

instance Arbitrary LargeInteger where
  arbitrary = do
    sign <- arbitrary
    LargeNonNegative n <- arbitrary
    return $ LargeInteger $ if sign then negate n else n

prop_intLog2 (LargePositive n) = 2 ^ l <= n && n < 2 ^ (l+1)
  where l = intLog2 n

prop_intLog (LargePositive n) (Positive b) = b < 2 || b ^ l <= n && n < b ^ (l+1)
  where l = intLog b n

prop_nbits (LargePositive n) = 2 ^ (l-1) <= n && n < 2 ^ l
  where l = nbits n

-- prop_digitsInBase :: LargePositive -> Word8 -> Bool
-- prop_digitsInBase (LargePositive n) b' = b' < 2 || b ^ (l-1) <= n && n < b ^ l
--   where
--     b = fromIntegral b'
--     l = digitsInBase (fromIntegral b) n

prop_sqrtRem (LargeNonNegative n) = r^2 + b == n
  where (r,b) = intSqrtRem n
prop_rootRem (LargeNonNegative n) (Positive e) = r^e + b == n
  where (r,b) = intRootRem e n

prop_nextPrime (LargeNonNegative n) =
  np > n && [np] == filter isPrime [n+1 .. np]
    where np = nextPrime n
prop_prevPrime (LargeNonNegative n) =
  (n <= 3 && pp == 2) || (pp < n && [pp] == filter isPrime [pp .. n-1])
    where pp = prevPrime n

prop_invMod (LargePositive a) (LargePositive m)
  | m < 2 = True
  | gcd a m /= 1 = invMod a m == 0
  | otherwise = a' >= 1 && a' < m && a' * a `mod` m == 1
  where a' = invMod a m

powModArgs = do
  LargePositive a' <- arbitrary
  LargeInteger e <- arbitrary
  LargePositive m <- arbitrary `suchThat` (> LargePositive a')
  return (makeCoprime m a', e, m)

prop_powMod = forAll powModArgs $ \(a,e,m) -> let b = powMod a e m in
  all id [
    a * powMod a (e-1) m `mod` m == b,
    b * powMod a (-e) m `mod` m == 1,
    powMod a (e+1) m == b * a `mod` m]

prop_intSqrtRem (LargeNonNegative n) = r^2 + b == n
  where (r, b) = intSqrtRem n

prop_intRootRem (LargeNonNegative n) (Positive e) = r^e + b == n
  where (r, b) = intRootRem e n

prop_intTrailingZeros (LargeInteger n) = (n == 0 && k == -1) || (testBit n k && n Data.Bits..&. (2^k - 1) == 0)
  where k = intTrailingZeros n

arbFactor = arbitrary `suchThat` ((> 1) . abs)
arbNonZero = arbitrary `suchThat` ((> LargeInteger 0) . abs)

prop_divPow = forAll ((,) <$> arbitrary <*> arbFactor)
  $ \(LargeInteger n, b) -> let (q, e) = divPow b n in n == q * b^e

prop_oddPow (LargeInteger n) = n == q * 2^e
  where (q, e) = oddPow n

return [] -- see comment in QuickCheck docs for quickCheckAll
runTests = $quickCheckAll

main :: IO ()
main = do
  runTests
  return ()
